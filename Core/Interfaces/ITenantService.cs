﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ITenantService
    {
        public Task<Tenant> GetByIdAsync(int id);
        public Task<Tenant> CreateAsync(Tenant tenant);
        public Task RunMigrations(Tenant tenant);
    }
}
