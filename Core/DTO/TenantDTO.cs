﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTO
{
    public class TenantDTO
    {
        public string Name { get; set; }
        public string ConnectionString { get; set; }
    }
}
