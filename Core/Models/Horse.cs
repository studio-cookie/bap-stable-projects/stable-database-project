﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Horse : BaseEntity
    {
        public Horse(string name)
        {
            Name = name;
        }
        protected Horse()
        {
        }
        public string Name { get; private set; }

    }
}
