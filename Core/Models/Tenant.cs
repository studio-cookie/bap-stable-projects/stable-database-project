﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Models
{
    public class Tenant: BaseEntity
    {
        public string Name { get; set; }
        public string ConnectionString { get; set; }
    }
}
