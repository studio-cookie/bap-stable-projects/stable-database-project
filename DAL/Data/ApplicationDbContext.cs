﻿using Core.Interfaces;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Data
{
    public class ApplicationDbContext : DbContext
    {

        private readonly IAuthService _authService;
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IAuthService authService) : base(options)
        {
            _authService = authService;
        }

        public DbSet<Horse> Horses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(_authService != null)
            {
                var tenantConnectionString = _authService.GetConnectionString();
                if (!string.IsNullOrEmpty(tenantConnectionString))
                {
                    var DBProvider = _authService.GetDatabaseProvider();
                    if (DBProvider.ToLower() == "mssql")
                    {
                        optionsBuilder.UseSqlServer(_authService.GetConnectionString());
                    }
                }
            }
        }  
    }
}
