﻿using Core.Interfaces;
using Core.Models;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class TenantService : ITenantService
    {
        private readonly TenantDbContext _tenantContext;

        public TenantService(TenantDbContext tenantContext)
        {
            _tenantContext = tenantContext;
        }

        public async Task<Tenant> GetByIdAsync(int id)
        {
            return await _tenantContext.Tenants.FindAsync(id);
        }

        public async Task<Tenant> CreateAsync(Tenant tenant)
        {
            _tenantContext.Add(tenant);
            await _tenantContext.SaveChangesAsync();

            return tenant;
        }

        public async Task RunMigrations(Tenant tenant)
        {
            DbContextOptionsBuilder<ApplicationDbContext> optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(tenant.ConnectionString);

            

            ApplicationDbContext context = new ApplicationDbContext(optionsBuilder.Options, null);
            await context.Database.GetService<IMigrator>().MigrateAsync();
        }
        
    }
}
